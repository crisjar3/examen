﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen
{
    class Tabla
    {
        int No_Cuota { get; set; }
        double Principal { get; set; }
        DateTime Fecha { get; set; }
        double Intereses { get; set; }
        double Cuota { get; set; }
        double Saldo_Principal { get; set; }

    }
}
