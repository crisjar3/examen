﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen
{
    public partial class Form1 : Form
    {
        Calculo cal;
        private DataTable dtProductos;
        public Form1()
        {
            InitializeComponent();
            cal = new Calculo();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            Tabla y = new Tabla();
            double monto, inte, plazo;
            if(mtxtMonto.Text.Equals("") || txtInteres.Text.Equals("")|| txtPlazo.Text.Equals(""))
            {
                MessageBox.Show(this, "ERROR, debe rellenar todos los campos", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                monto = double.Parse(mtxtMonto.Text);
                inte = double.Parse(txtInteres.Text);
                plazo = double.Parse(txtPlazo.Text);
                double pmt =cal.Create(plazo,inte,monto);

                int n = Int32.Parse(txtPlazo.Text);

                for (int i = 0; i < n; i++) 
                {
                    DataRow drnE = dsPrestamo.Tables["Tabla"].NewRow();
                    drnE["No_Cuota"] = i++  ;
                    drnE["Intereses"] = inte;
                    drnE["Principal"] = monto;
                    drnE["Cuota"] = pmt;
                    drnE["Saldo_Principal"] = monto-pmt;
                    drnE["Fecha"] = dtpFecha.Value;
                    dsPrestamo.Tables["Tabla"].Rows.Add(drnE);
                    //profe si ve esto, no lo termine porque visual no me deja agregar el reporviewer a la clase, verifique los nuget y nada

                }

                

            }
        }
    }
}
